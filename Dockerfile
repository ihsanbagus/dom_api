FROM php:5.6.21-apache AS website
RUN apt-get update && \
	apt-get install -y nano libpng-dev zip zlib1g-dev libzip-dev libbz2-dev libmcrypt-dev && \
	a2enmod rewrite && \
	docker-php-ext-install mysqli pdo pdo_mysql gd zip bcmath bz2 calendar gettext exif mcrypt
EXPOSE 80