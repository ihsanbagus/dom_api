<?php
/**
 * File Name : Home.php
 * Editor: Ihsan Bagus
 * User: Hero
 * Date: 20 March 2019
 * Time: 4:50 PM
 * Build with : PhpStorm
 */
defined('BASEPATH') OR exit('No direct script access allowed');

class Home extends MY_Controller
{

    public function __construct()
    {
        parent::__construct();

        //Do your magic here
    }

    public function index()
    {
    }

}

/* End of file Home.php */
