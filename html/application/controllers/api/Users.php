<?php

/**
 * File Name : Users.php
 * Editor: Ihsan Bagus
 * User: Hero
 * Date: 20 March 2019
 * Time: 4:50 PM
 * Build with : PhpStorm
 */
defined('BASEPATH') or exit('No direct script access allowed');

class Users extends MY_Api
{

    public function __construct()
    {
        parent::__construct();
        header('Content-Type: application/json');
        $this->load->model('User_model', 'user');
        $this->load->helper('return_data');
    }

    public function index_get()
    {
        echo 'hai';
    }

    public function index_post()
    {
        $users = $this->user->getAllUser();
        $return = return_data($users);
        echo json_encode($return);
    }

    public function me_post()
    {
        $nip = $this->input->post('nip');
        $user = $this->user->getEmploye(['nip' => $nip]);
        $return = return_data($user);
        echo json_encode($return);
    }
}

/* End of file Users.php */
