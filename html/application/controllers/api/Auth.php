<?php

/**
 * File Name : Users.php
 * Editor: Ihsan Bagus
 * User: Hero
 * Date: 20 March 2019
 * Time: 4:50 PM
 * Build with : PhpStorm
 */
defined('BASEPATH') or exit('No direct script access allowed');

class Auth extends MY_Api
{

    public function __construct()
    {
        parent::__construct();
        header('Content-Type: application/json');
        $this->load->model('User_model', 'user');
        $this->load->helper('return_data');
    }

    public function index_get()
    {
        echo 'hai';
    }

    public function login_post()
    {
        $nip = $this->input->post('nip');
        $password = $this->input->post('password');

        $param = array('nip' => $nip, 'password' => $password, 'allow' => 'Y');
        $user = $this->user->getUser($param);

        if ($user == null) {
            $return = return_data(null, false, 404, 'Data Tidak Ditemukan');
        } else {
            $return = return_data($user);
        }

        echo json_encode($return);
    }
}

/* End of file Users.php */
