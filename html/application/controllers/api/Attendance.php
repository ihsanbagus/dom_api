<?php

/**
 * File Name : Users.php
 * Editor: Ihsan Bagus
 * User: Hero
 * Date: 20 March 2019
 * Time: 4:50 PM
 * Build with : PhpStorm
 */
defined('BASEPATH') or exit('No direct script access allowed');

class Attendance extends MY_Api
{

    public function __construct()
    {
        parent::__construct();
        header('Content-Type: application/json');
        $this->load->model('Time_model', 'time');
        $this->load->model('Attendance_model', 'attendance');
        $this->load->helper('return_data');
    }

    public function index_get()
    {
        echo 'hai';
    }

    public function absenin_post()
    {
        $nip = $this->input->post('nip');
        $date = date('Y-m-d');
        $time = date('H:i:s');

        //cek apakah sudah absen masuk?
        $masuk = $this->attendance->getHistory(['nip' => $nip, 'date' => $date, 'check_in' => '']);
        if (count($masuk) < 1) {
            $absen = $this->attendance->absenIn(['nip' => $nip, 'date' => $date, 'check_in' => $time]);
            $return = return_data($absen, true, 200, "Anda Berhasil Absen Masuk");
        } else {
            $return = return_data($masuk[0], false, 404, 'Anda Sudah Absen Masuk');
        }
        echo json_encode($return);
    }

    public function absenout_post()
    {
        $nip = $this->input->post('nip');
        $date = date('Y-m-d');
        $time = date('H:i:s');

        //cek apa sudah absen masuk dahulu?
        $masuk = $this->attendance->getHistory(['nip' => $nip, 'date' => $date]);
        if (count($masuk) < 1) {
            $return = return_data(null, false, 404, 'Anda Belum Absen Masuk');
        } else {
            //cek apa sudah absen keluar?
            $cek = $this->attendance->getHistory(['nip' => $nip, 'date' => $date, 'check_out' => null]);
            if ($masuk[0]->check_out == '') {
                $this->attendance->absenOut(['nip' => $nip, 'date' => $date], ['check_out' => $time]);
                $absen = $this->attendance->getHistory(['nip' => $nip, 'date' => $date])[0];
                $return = return_data($absen, true, 200, "Anda Berhasil Absen Keluar");
            } else {
                $return = return_data($cek[0], false, 404, 'Anda Sudah Absen Keluar');
            }
        }

        echo json_encode($return);
    }

    public function history_post()
    {
        $nip = $this->input->post('nip');
        $date = $this->input->post('date');

        if ($nip == null || $date == null) {
            $return = return_data(null, false, 404, 'Data Tidak Ditemukan');
        } else {
            $time = $this->time->getTimeIn();
            $user = $this->attendance->getHistory(['nip' => $nip, 'date' => $date]);
            if (count($user) > 0) {
                $month = substr($date, 5, 2);
                $year = substr($date, 0, 4);
                $day = cal_days_in_month(CAL_GREGORIAN, $month, $year);

                // for ($i = 0; $i < $day; $i++) {
                //     if ($user[$i]->check_in > $time->start) {
                //         $user[$i] = $user[$i];
                //     }
                // }

                foreach ($user as $k => $v) {
                    if ($v->check_in > $time->start) {
                        $user[$k]->status = '1';
                    } else {
                        $user[$k]->status = '0';
                    }
                    $user[$k]->time_in = $time->start;
                }
            }
            $return = return_data($user);
        }
        echo json_encode($return);
    }
}

/* End of file Users.php */
