<?php

/**
 * File Name : Abouts.php
 * Editor: Ihsan Bagus
 * About: Hero
 * Date: 20 March 2019
 * Time: 4:50 PM
 * Build with : PhpStorm
 */
defined('BASEPATH') or exit('No direct script access allowed');

class Memo extends MY_Api
{
    public function __construct()
    {
        parent::__construct();
        header('Content-Type: application/json');
        $this->load->model('Memo_model', 'memo');
        $this->load->helper('return_data');
    }

    public function index_get()
    {
        echo 'hai';
    }

    public function index_post()
    {
        $nip = $this->input->post('nip');
        $memo = $this->memo->getDatas(['nip' => $nip]);
        $return = return_data($memo);
        echo json_encode($return);
    }

    public function listPenerima_post()
    {
        $nip = $this->input->post('nip');
        $memo = $this->memo->listPenerima(['nip' => $nip]);
        $return = return_data($memo);
        echo json_encode($return);
    }

    public function add_post()
    {
        $sender = $this->input->post('sender');
        $receiver = $this->input->post('receiver');
        $title = $this->input->post('title');
        $body = $this->input->post('body');
        $attachment = $this->input->post('attachment');
        // print_r($attachment);die();

        // $config['upload_path']          = FCPATH . 'uploads/';
        // $config['allowed_types']        = 'gif|jpg|png';
        // $config['max_size']             = 107855;
        // $config['max_width']            = 1024;
        // $config['max_height']           = 768;

        // $this->load->library('upload', $config);

        // if (!$this->upload->do_upload('attachment')) {
        //     $error = array('error' => $this->upload->display_errors());
        //     print_r($error);
        //     die;
        // } else {
        //     // $data = array('upload_data' => $this->upload->data());
        //     $data = $this->upload->data();
        //     print_r($data['file_name']);
        //     die();
        //     // $file_name = $data['file_name'];
        // }


        $datas = [
            'sender' => $sender,
            'receiver' => $receiver,
            'title' => $title,
            'body' => $body,
            'attachment' => $attachment,
        ];

        $memo = $this->memo->addMemo($datas);
        $return = return_data($memo, true, 200, "Berhasil Mengirim");
        echo json_encode($return);
    }
}

/* End of file Abouts.php */
