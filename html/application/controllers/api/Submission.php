<?php

/**
 * File Name : Submissinos.php
 * Editor: Ihsan Bagus
 * Submissino: Hero
 * Date: 20 March 2019
 * Time: 4:50 PM
 * Build with : PhpStorm
 */
defined('BASEPATH') or exit('No direct script access allowed');

class Submission extends MY_Api
{

    public function __construct()
    {
        parent::__construct();
        header('Content-Type: application/json');
        $this->load->model('Submission_model', 'submission');
        $this->load->helper('return_data');
    }

    public function index_get()
    {
        echo 'hai';
    }

    public function index_post()
    {
        $nip = $this->input->post('nip');
        $submission = $this->submission->getData(['nip' => $nip]);
        $return = return_data($submission);
        echo json_encode($return);
    }

    public function add_post()
    {
        $sender = $this->input->post('sender');
        $receiver = $this->input->post('receiver');
        $title = $this->input->post('title');
        $body = $this->input->post('body');
        $mulai = $this->input->post('mulai');
        $akhir = $this->input->post('akhir');

        $upload = $this->upload();
        if ($upload['status'] == FALSE) {
            echo json_encode($upload);
            die();
        }

        $datas = [
            'user' => $sender,
            'approver' => $receiver,
            'submission_for' => $title,
            'user_desc' => $body,
            'implementation_date_start' => $mulai,
            'implementation_date_end' => $akhir,
            'attachment' => $upload['data']['file_name'],
            'status_sub' => 'P',
        ];

        $submission = $this->submission->addSubmission($datas);
        $return = return_data($submission, true, 200, "Berhasil Mengirim");
        echo json_encode($return);
    }

    public function upload()
    {
        $config['upload_path']          = FCPATH . 'uploads/';
        $config['allowed_types']        = 'jpg|jpeg|png|doc|docx|pdf';
        $config['max_size']             = 1024;
        $config['max_width']            = 1024;
        $config['max_height']           = 768;

        $this->load->library('upload', $config);
        $this->upload->initialize($config);

        if (!$this->upload->do_upload('attachment')) {
            $error = $this->upload->display_errors();
            $return = return_data(null, false, 404, $error);
            return $return;
        } else {
            $data = return_data($this->upload->data(), true, 200, "Berhasil Upload");
            return $data;
        }
    }
}

/* End of file Submissinos.php */
