<?php

/**
 * File Name : Abouts.php
 * Editor: Ihsan Bagus
 * About: Hero
 * Date: 20 March 2019
 * Time: 4:50 PM
 * Build with : PhpStorm
 */
defined('BASEPATH') or exit('No direct script access allowed');

class Time extends MY_Api
{

    public function __construct()
    {
        parent::__construct();
        header('Content-Type: application/json');
        $this->load->model('Time_model', 'time');
        $this->load->helper('return_data');
    }

    public function index_get()
    {
        echo 'hai';
    }

    public function in_post()
    {
        $data = $this->time->getTimeIn();
        $return = return_data($data);
        echo json_encode($return);
    }

    public function out_post()
    {
        $data = $this->time->getTimeOut();
        $return = return_data($data);
        echo json_encode($return);
    }
}

/* End of file Abouts.php */
