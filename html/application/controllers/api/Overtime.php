<?php

/**
 * File Name : Overtimes.php
 * Editor: Ihsan Bagus
 * Overtime: Hero
 * Date: 20 March 2019
 * Time: 4:50 PM
 * Build with : PhpStorm
 */
defined('BASEPATH') or exit('No direct script access allowed');

class Overtime extends MY_Api
{

    public function __construct()
    {
        parent::__construct();
        header('Content-Type: application/json');
        $this->load->model('Overtime_model', 'overtime');
        $this->load->helper('return_data');
    }

    public function index_get()
    {
        echo 'hai';
    }

    public function index_post()
    {
        $nip = $this->input->post('nip');

        if ($nip == null) {
            $return = return_data(null, false, 404, 'Data Tidak Ditemukan');
        } else {
            $data = $this->overtime->getHistory($nip);
            $return = return_data($data);
        }
        echo json_encode($return);
    }

    public function add_post()
    {
        $sender = $this->input->post('sender');
        $receiver = $this->input->post('receiver');
        $body = $this->input->post('body');
        $tanggal = $this->input->post('tanggal');

        $datas = [
            'user' => $sender,
            'approver' => $receiver,
            'implementation_date' => $tanggal,
            'submission_date' => date('Y-m-d H:i:s'),
            'user_desc' => $body,
            'status_sub_ot' => 'P'
        ];

        $overtime = $this->overtime->addOvertime($datas);
        $return = return_data($overtime, true, 200, "Berhasil Mengirim");
        echo json_encode($return);
    }
}

/* End of file Overtimes.php */
