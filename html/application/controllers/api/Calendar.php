<?php

/**
 * File Name : Calendars.php
 * Editor: Ihsan Bagus
 * Calendar: Hero
 * Date: 20 March 2019
 * Time: 4:50 PM
 * Build with : PhpStorm
 */
defined('BASEPATH') or exit('No direct script access allowed');

class Calendar extends MY_Api
{

    public function __construct()
    {
        parent::__construct();
        header('Content-Type: application/json');
        $this->load->model('Calendar_model', 'calendar');
        $this->load->helper('return_data');
    }

    public function index_get()
    {
        echo 'hai';
    }

    public function index_post()
    {
        $calendar = $this->calendar->getData();
        $return = return_data($calendar);
        echo json_encode($return);
    }
}

/* End of file Calendars.php */
