<?php

/**
 * File Name : Abouts.php
 * Editor: Ihsan Bagus
 * About: Hero
 * Date: 20 March 2019
 * Time: 4:50 PM
 * Build with : PhpStorm
 */
defined('BASEPATH') or exit('No direct script access allowed');

class About extends MY_Api
{

    public function __construct()
    {
        parent::__construct();
        header('Content-Type: application/json');
        $this->load->model('About_model', 'about');
        $this->load->helper('return_data');
    }

    public function index_get()
    {
        echo 'hai';
    }

    public function index_post()
    {
        $About = $this->about->getData();
        $return = return_data($About);
        echo json_encode($return);
    }
}

/* End of file Abouts.php */
