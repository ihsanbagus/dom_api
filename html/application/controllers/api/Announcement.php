<?php

/**
 * File Name : Users.php
 * Editor: Ihsan Bagus
 * User: Hero
 * Date: 20 March 2019
 * Time: 4:50 PM
 * Build with : PhpStorm
 */
defined('BASEPATH') or exit('No direct script access allowed');

class Announcement extends MY_Api
{

    public function __construct()
    {
        parent::__construct();
        header('Content-Type: application/json');
        $this->load->model('Announcement_model', 'announcement');
        $this->load->helper('return_data');
    }

    public function index_get()
    {
        echo 'hai';
    }

    public function index_post()
    {
        $datas = $this->announcement->getAll();
        $return = return_data($datas);
        echo json_encode($return);
    }
}

/* End of file Users.php */
