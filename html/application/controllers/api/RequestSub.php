<?php

/**
 * File Name : Abouts.php
 * Editor: Ihsan Bagus
 * About: Hero
 * Date: 20 March 2019
 * Time: 4:50 PM
 * Build with : PhpStorm
 */
defined('BASEPATH') or exit('No direct script access allowed');

class RequestSub extends MY_Api
{

    public function __construct()
    {
        parent::__construct();
        header('Content-Type: application/json');
        $this->load->model('RequestSub_model', 'req');
        $this->load->helper('return_data');
    }

    public function index_get()
    {
        echo 'hai';
    }

    public function index_post()
    {
        $nip = $this->input->post('nip');
        $sub = $this->req->getSubmission(['approver' => $nip]);
        $ovr = $this->req->getOvertiome(['approver' => $nip]);

        foreach ($sub as $k => $v) {
            $v->type = "submission";
        }
        foreach ($ovr as $k => $v) {
            $v->type = "overtime";
            array_push($sub, $v);
        }

        $return = return_data($sub);
        echo json_encode($return);
    }
}

/* End of file Abouts.php */
