<?php
defined('BASEPATH') OR exit('No direct script access allowed');

if ( ! function_exists('return_data'))
{
    function return_data($datas, $status = true, $code = 200, $message = 'berhasil')
    {
        return [
            "status" => $status,
            "code"	=> $code,
            "message" => $message,
            "data"	=> $datas
        ];
    }   
}