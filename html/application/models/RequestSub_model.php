<?php
defined('BASEPATH') or exit('No direct script access allowed');

class RequestSub_model extends CI_Model
{

    private $submission = 'disc_submission';
    private $overtime = 'overtime_submission';

    public function getSubmission($param)
    {
        $qs = "SELECT e.first_name, e.last_name, s.* 
            FROM disc_submission s
            JOIN employe e
            ON s.user = e.nip
            WHERE s.approver = $param[approver]";

        return $this->db->query($qs)->result();
        // return $this->db->get_where($this->submission, $param)->result();
    }

    public function getOvertiome($param)
    {
        $qo = "SELECT e.first_name, e.last_name, s.* 
            FROM overtime_submission s
            JOIN employe e
            ON s.user = e.nip
            WHERE s.approver = $param[approver]";

        return $this->db->query($qo)->result();
        // return $this->db->get_where($this->submission, $param)->result();
    }
}
