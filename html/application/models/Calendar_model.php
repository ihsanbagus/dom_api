<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Calendar_model extends CI_Model
{

    private $calendar = 'calendar';

    public function getData()
    {
        $this->db->order_by('id', 'DESC');
        return $this->db->get($this->calendar)->result();
    }
}
