<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Memo_model extends CI_Model
{

    private $memo = 'memo';
    private $employe = 'employe';

    public function getDatas($param)
    {
        $q = "SELECT e.first_name, e.last_name, m.* 
            FROM memo m 
            JOIN employe e ON m.sender = e.nip 
            WHERE m.receiver = $param[nip]
            LIMIT 10";
        $query = $this->db->query($q);
        return $query->result();
        // $this->db->select('*');
        // $this->db->from($this->memo);
        // $this->db->join($this->user, $this->memo . '.id = ' . $this->user . '.id');
        // $this->db->where(['sender' => $nip]);        
        // return $this->db->get()->result();
        // return $this->db->get_where($this->memo, array('sender' => $nip))->result();
    }

    public function listPenerima($param)
    {
        $q = "SELECT nip, first_name, last_name, position FROM employe WHERE nip != $param[nip]";
        return $this->db->query($q)->result();
    }

    public function addMemo($datas)
    {
        $this->db->insert($this->memo, $datas);
        return $this->db->get_where($this->memo, ['id' => $this->db->insert_id()])->row();
    }
}
