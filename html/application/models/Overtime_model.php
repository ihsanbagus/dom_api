<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Overtime_model extends CI_Model
{

    private $overtime = 'overtime_submission';

    public function getHistory($nip)
    {
        $q = "SELECT e.first_name, e.last_name, o.* FROM overtime_submission o
            JOIN employe e
            ON o.approver = e.nip
            WHERE o.user = '$nip'";
        return $this->db->query($q)->result();
        // return $this->db->get_where($this->overtime, array('user' => $nip))->result();
    }

    public function addOvertime($datas)
    {
        $this->db->insert($this->overtime, $datas);
        return $this->db->get_where($this->overtime, ['id_sub_ot' => $this->db->insert_id()])->row();
    }
}
