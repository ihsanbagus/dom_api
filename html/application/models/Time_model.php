<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Time_model extends CI_Model
{

    private $table = 'time';

    public function getTimeIn()
    {
        return $this->db->get_where($this->table, array('status' => 'in'))->row();
    }

    public function getTimeOut()
    {
        return $this->db->get_where($this->table, array('status' => 'out'))->row();
    }
}
