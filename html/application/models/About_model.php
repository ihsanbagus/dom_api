<?php
defined('BASEPATH') or exit('No direct script access allowed');

class About_model extends CI_Model
{

    private $About = 'about_us';

    public function getData($id = '1')
    {
        return $this->db->get_where($this->About, array('id' => $id))->row();
    }
}
