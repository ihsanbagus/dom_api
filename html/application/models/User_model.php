<?php
defined('BASEPATH') or exit('No direct script access allowed');

class User_model extends CI_Model
{

    private $user = 'user';
    private $employe = 'employe';

    public function getUser($param)
    {
        return $this->db->get_where($this->user, $param)->row();
    }

    public function getEmploye($param)
    {
        $q = "SELECT e.*, d.division_name, p.position_name 
            FROM employe AS e 
            JOIN division AS d
            ON e.division = d.id
            JOIN position AS p
            ON e.position = p.id
            WHERE e.nip = '$param[nip]'";
        return $query = $this->db->query($q)->row();
        // return $this->db->get_where($this->employe, $param)->row();
    }

    public function getAllUser()
    {
        return $this->db->get($this->user)->result();
    }
}
