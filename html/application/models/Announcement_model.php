<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Announcement_model extends CI_Model {

    private $announcement = 'announcement';

    public function getAll()
    {
        $this->db->order_by('id', 'desc');
        return $this->db->get($this->announcement, 3)->result();
    }
}