<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Attendance_model extends CI_Model
{

    private $attendance = 'attendance';

    public function getHistory($param)
    {
        $this->db->like('date', $param['date']);
        return $this->db->get_where($this->attendance, ['nip' => $param['nip']])->result();
    }

    public function absenIn($param)
    {
        $this->db->insert($this->attendance, $param);
        return $this->db->get_where($this->attendance, ['id' => $this->db->insert_id()])->row();
    }

    public function absenOut($param, $data)
    {
        $this->db->set($data);
        $this->db->where($param);
        $this->db->update($this->attendance);
        return $this->db->get_where($this->attendance, ['id' => $this->db->insert_id()])->row();
    }
}
