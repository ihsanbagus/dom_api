<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Submission_model extends CI_Model
{

    private $submission = 'disc_submission';

    public function getData($param)
    {
        $q = "SELECT s.*, e.first_name, e.last_name 
            FROM disc_submission AS s
            JOIN employe AS e
            ON s.approver = e.nip
            WHERE s.user = $param[nip]
            LIMIT 5";
        return $this->db->query($q)->result();
        // return $this->db->get_where($this->submission, $param)->result();
    }

    public function addSubmission($datas)
    {
        $this->db->insert($this->submission, $datas);
        return $this->db->get_where($this->submission, ['id_sub' => $this->db->insert_id()])->row();
    }
}
