<?php
/**
 * File Name : MY_Api.php
 * Editor: Ihsan Bagus
 * User: Hero
 * Date: 20 March 2019
 * Time: 4:41 PM
 * Build with : PhpStorm
 */

use Restserver\Libraries\REST_Controller;

defined('BASEPATH') OR exit('No direct script access allowed');

require APPPATH . 'libraries/REST_Controller.php';
require APPPATH . 'libraries/Format.php';

class MY_Api extends REST_Controller
{

}

/* End of file MY_Api.php */
