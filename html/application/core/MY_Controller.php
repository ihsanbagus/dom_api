<?php
/**
 * File Name : MY_Controller.php
 * Editor: Ihsan Bagus
 * User: Hero
 * Date: 20 March 2019
 * Time: 3:58 PM
 * Build with : PhpStorm
 */

defined('BASEPATH') OR exit('No direct script access allowed');

class MY_Controller extends CI_Controller
{

	public function __construct()
	{
		parent::__construct();
		$this->config->set_item('language', 'english');
	}

}

/* End of file MY_Controller.php */
